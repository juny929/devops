"use strict";

const express = require("express");
const lineReader = require("line-reader");
const Promise = require("bluebird");

// Constants
const PORT = 8000;
const HOST = "0.0.0.0";

// App
const app = express();
app.get("/", (req, res) => {
  const eachLine = Promise.promisify(lineReader.eachLine);
  eachLine("/data/file/output.txt", function (line) {
    console.log(line);
    res.write(line + "\n");
  })
    .then(function () {
      console.log("done");
      res.end();
    })
    .catch(function (err) {
      console.error(err);
      res.sendStatus(500);
    });
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
