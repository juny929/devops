#!/usr/bin/env node

"use strict";

var amqp = require("amqplib");

module.exports.emitTopic = function (rabbitHost, key, msg) {
  amqp.connect("amqp://" + rabbitHost).then(function (c) {
    c.createConfirmChannel().then(function (ch) {
      const exchange = "topic_logs";
      ch.assertExchange(exchange, "topic", { durable: false });
      ch.publish(exchange, key, Buffer.from(msg));
      console.log(" [x] Sent %s:'%s'", key, msg);
    });
  });
};
