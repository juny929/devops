#!/usr/bin/env node

"use strict";

var amqp = require("amqplib");

module.exports.receiveTopic = function (rabbitHost, keys, callback) {
  amqp
    .connect("amqp://" + rabbitHost)
    .then(function (conn) {
      process.once("SIGINT", function () {
        conn.close();
      });
      return conn.createChannel().then(function (ch) {
        const exchange = "topic_logs";
        ch.assertExchange(exchange, "topic", { durable: false });
        var ok = ch.assertQueue("", { exclusive: true });
        ok = ok.then(function (q) {
          console.log(
            new Date(),
            " [*] Waiting for messages. To exit press CTRL+C"
          );
          keys.forEach((key) => ch.bindQueue(q.queue, exchange, key));
          ch.consume(q.queue, doWork, { noAck: false });
        });
        return ok;

        function doWork(msg) {
          const body = msg.content.toString();
          const routingKey = msg.fields.routingKey;
          console.log(" [x] %s:'%s'", routingKey, body);
          ch.ack(msg);
          callback(null, body, routingKey);
        }
      });
    })
    .catch(console.warn);
};
