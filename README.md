# 5. Message queue

## 5.1. Message queue

Request-response communication requires both client and server to be connected.
If either one is disconnected, the communication is failed.
In addition, client always needs to send request to check updates from server.

On the other hand, with topic-based communication, publisher just emits new message to message broker and it will automatically transmit it to corresponding subscriber who is listening the certain topic.
In addition, if the message broker is alive, it keeps messages in the corresponding topic queues until a subscriber of the topic appears.

I used Nodejs to complete this task.
I have learned how to run RabbitMQ using docker container.
Also, I studied how to send and receive a message via the message broker with different topics.

In addition, I understand more about docker compose such as building internal network and sharing data using volumes.

### Get started

```
docker-compose build --no-cache
docker-compose up

curl http://localhost:8080

```