"use strict";

const receive = require("./utils/receive_log_topic");
const fs = require("fs");

const host = process.env.RABBITMQ_HOST || "localhost:5672";

fs.writeFileSync("/data/file/output.txt", "");

receive.receiveTopic(host, ["my.o", "my.i"], function (err, body, topic) {
  // format received
  const timestamp = new Date().toISOString();
  const message = body;
  const text = `${timestamp} Topic ${topic}: ${message}\n`;

  // write file
  fs.appendFileSync("/data/file/output.txt", text);
});
