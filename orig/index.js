"use strict";

const emit = require("./utils/emit_log_topic");

const host = process.env.RABBITMQ_HOST || "localhost:5672";

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const orig = async () => {
  let i = 0;
  await wait(3000);
  emit.emitTopic(host, "my.o", `MSG_${++i}`);
  await wait(3000);
  emit.emitTopic(host, "my.o", `MSG_${++i}`);
  await wait(3000);
  emit.emitTopic(host, "my.o", `MSG_${++i}`);
};

orig();
