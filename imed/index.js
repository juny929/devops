"use strict";

const receive = require("./utils/receive_log_topic");
const emit = require("./utils/emit_log_topic");

const host = process.env.RABBITMQ_HOST || "localhost:5672";

receive.receiveTopic(host, "my.o", function (err, body) {
  setTimeout(() => {
    const message = `Got ${body}`;
    // publish message
    emit.emitTopic(host, "my.i", message);
  }, 1000);
});
